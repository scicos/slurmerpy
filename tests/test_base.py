from unittest import TestCase, mock
import sys
import os
import tempfile

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from slurmer import Slurmer, SlurmerProvider


class TestSlurmer(TestCase):
    def test_from_dict(self):
        runner = Slurmer.from_dict(
            {
                "hostname": "http://localhost:8080",
                "apps": [
                    {
                        "name": "test",
                        "access_token": "PssEpTGgFcfAspkqudabvi3UjCSRRbfLBGBhGa2f428=",
                        "id": "10ec28fb-93be-48c2-ac3d-f3bf464eb2cc",
                    }
                ],
            }
        )

        self.assertEqual(runner.hostname, "http://localhost:8080")
        self.assertEqual(runner.apps["test"].name, "test")
        self.assertEqual(
            runner.apps["test"].access_token,
            "PssEpTGgFcfAspkqudabvi3UjCSRRbfLBGBhGa2f428=",
        )
        self.assertEqual(
            runner.apps["test"].id, "10ec28fb-93be-48c2-ac3d-f3bf464eb2cc"
        )

    def test_from_dict_missing_keys(self):
        with self.assertRaises(KeyError):
            Slurmer.from_dict({})
        with self.assertRaises(KeyError):
            Slurmer.from_dict({"hostname": "http://localhost:8080"})
        with self.assertRaises(KeyError):
            Slurmer.from_dict({"apps": []})

    @mock.patch("slurmer.provider.requests.request")
    def test_provider_request_get(self, mock_request):
        runner = SlurmerProvider(
            "http://localhost:8080", "truc", "thisisatoken"
        )
        runner.get("urlendpoint", stream=True)

        mock_request.assert_called_with(
            "GET",
            "http://localhost:8080/truc/urlendpoint",
            headers={"X-Auth-Token": "thisisatoken"},
            json=None,
            files=None,
            stream=True,
        )

    @mock.patch("slurmer.provider.requests.request")
    def test_provider_request_post(self, mock_request):
        runner = SlurmerProvider("http://localhost:8080", "", "thisisatoken")
        runner.post("urlendpoint", {"data": "data"})

        mock_request.assert_called_with(
            "POST",
            "http://localhost:8080/urlendpoint",
            headers={"X-Auth-Token": "thisisatoken"},
            json={"data": "data"},
            files=None,
            stream=False,
        )

    @mock.patch("slurmer.provider.requests.request")
    def test_provider_request_put(self, mock_request):
        runner = SlurmerProvider("http://localhost:8080", "", "thisisatoken")
        runner.put("urlendpoint", {"data": "data"})

        mock_request.assert_called_with(
            "PUT",
            "http://localhost:8080/urlendpoint",
            headers={"X-Auth-Token": "thisisatoken"},
            json={"data": "data"},
            files=None,
            stream=False,
        )

    @mock.patch("slurmer.provider.requests.request")
    def test_provider_request_delete(self, mock_request):
        runner = SlurmerProvider("http://localhost:8080", "", "thisisatoken")
        runner.delete("urlendpoint")

        mock_request.assert_called_with(
            "DELETE",
            "http://localhost:8080/urlendpoint",
            headers={"X-Auth-Token": "thisisatoken"},
            json=None,
            files=None,
            stream=False,
        )

    def test_from_config(self):
        with tempfile.NamedTemporaryFile("w") as f:
            f.write(
                """{
                    "hostname": "http://localhost:8080",
                    "apps": [
                        {
                            "name": "test",
                            "access_token": "PssEpTGgFcfAspkqudabvi3UjCSRRbfLBGBhGa2f428=",
                            "id": "10ec28fb-93be-48c2-ac3d-f3bf464eb2cc"
                        }
                    ]
                }"""
            )
            f.seek(0)
            runner = Slurmer.from_config(f.name)

        self.assertEqual(runner.hostname, "http://localhost:8080")
        self.assertEqual(runner.apps["test"].name, "test")
        self.assertEqual(
            runner.apps["test"].access_token,
            "PssEpTGgFcfAspkqudabvi3UjCSRRbfLBGBhGa2f428=",
        )
        self.assertEqual(
            runner.apps["test"].id, "10ec28fb-93be-48c2-ac3d-f3bf464eb2cc"
        )
