import requests
from unittest import mock, TestCase
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from slurmer import Slurmer


class TestSlurmerApp(TestCase):
    def setUp(self):
        self.runner = Slurmer.from_dict(
            {
                "hostname": "http://localhost:8080",
                "apps": [
                    {
                        "name": "test",
                        "access_token": "AcCeSsToKeN",
                        "id": "10ec28fb-93be-48c2-ac3d-f3bf464eb2cc",
                    }
                ],
            }
        )
        self.app = self.runner.apps["test"]
        self.job_infos = {
            "b335e27d-18d9-4612-b1a1-d3a522ef6c16": {
                "id": "b335e27d-18d9-4612-b1a1-d3a522ef6c16",
                "name": "job1",
                "status": "running",
                "slurm_id": 1234,
                "slurm_job": {
                    "job_id": 1234,
                    "name": "job1",
                    "job_state": "RUNNING",
                    "state_description": "Job is running",
                    "state_reason": "",
                    "account": "test",
                    "user_name": "test",
                },
            },
            "24d718d7-0301-4e9d-a333-b87e8694a1d4": {
                "id": "24d718d7-0301-4e9d-a333-b87e8694a1d4",
                "name": "job2",
                "status": "stopped",
                "slurm_id": 0,
                "slurm_job": None,
            },
        }

    def test_str(self):
        self.assertEqual(
            str(self.app),
            "App test (10ec28fb-93be-48c2-ac3d-f3bf464eb2cc)",
        )

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_is_up(self, mock_request):
        mock_request.return_value.status_code = 200
        mock_request.return_value.json.return_value = {
            "id": "10ec28fb-93be-48c2-ac3d-f3bf464eb2cc",
            "name": "test",
            "options": [],
            "take_command_args": True,
        }
        self.assertTrue(self.app.is_up())
        print(mock_request.call_args_list)
        mock_request.assert_called_with(
            "",
            "GET",
            stream=False,
            verbose=False,
        )

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_is_up_not_up(self, mock_request):
        mock_request.return_value.status_code = 404
        self.assertFalse(self.app.is_up())

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_is_up_connection_error(self, mock_request):
        mock_request.side_effect = requests.exceptions.ConnectionError()
        self.assertFalse(self.app.is_up())

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_is_up_other_error(self, mock_request):
        mock_request.return_value.status_code = 500
        self.assertFalse(self.app.is_up())

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_get_all_jobs(self, mock_request):
        mock_request.return_value.status_code = 200
        mock_request.return_value.json.return_value = list(
            self.job_infos.values()
        )
        jobs = self.app.get_all_jobs()
        self.assertEqual(len(jobs), 2)
        self.assertEqual(
            jobs["b335e27d-18d9-4612-b1a1-d3a522ef6c16"].name, "job1"
        )
        self.assertEqual(
            jobs["24d718d7-0301-4e9d-a333-b87e8694a1d4"].name, "job2"
        )

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_get_all_jobs_empty(self, mock_request):
        mock_request.return_value.status_code = 200
        mock_request.return_value.json.return_value = []
        jobs = self.app.get_all_jobs()
        self.assertEqual(len(jobs), 0)

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_get_job(self, mock_request):
        mock_request.return_value.status_code = 200
        mock_request.return_value.json.return_value = self.job_infos[
            "b335e27d-18d9-4612-b1a1-d3a522ef6c16"
        ]
        job = self.app.get_job("b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        self.assertEqual(job.name, "job1")
        self.assertEqual(job.status, "running")
        self.assertEqual(job.slurm_id, 1234)

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_create_job(self, mock_request):
        mock_request.return_value.status_code = 201
        mock_request.return_value.json.return_value = {
            "id": "ac35e27d-18d9-4612-b1a1-d3a522ef6c16",
            "name": "job1",
            "status": "running",
            "slurm_id": 0,
            "slurm_job": None,
        }
        job = self.app.create_job("srun", ["-n", "1"], {})
        self.assertEqual(job.name, "job1")
        self.assertEqual(job.status, "running")
        self.assertEqual(job.slurm_id, 0)
