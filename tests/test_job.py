import requests
from unittest import mock, TestCase
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from slurmer import Slurmer

import tempfile


class TestSlurmerJob(TestCase):
    def setUp(self):
        self.runner = Slurmer.from_dict(
            {
                "hostname": "http://localhost:8080",
                "apps": [
                    {
                        "name": "test",
                        "access_token": "AcCeSsToKeN",
                        "id": "10ec28fb-93be-48c2-ac3d-f3bf464eb2cc",
                    }
                ],
            }
        )
        self.app = self.runner.apps["test"]
        self.job_infos = {
            "b335e27d-18d9-4612-b1a1-d3a522ef6c16": {
                "id": "b335e27d-18d9-4612-b1a1-d3a522ef6c16",
                "name": "job1",
                "status": "running",
                "slurm_id": 1234,
                "slurm_job": {
                    "job_id": 1234,
                    "name": "job1",
                    "job_state": "RUNNING",
                    "state_description": "Job is running",
                    "state_reason": "",
                    "account": "test",
                    "user_name": "test",
                },
            },
            "24d718d7-0301-4e9d-a333-b87e8694a1d4": {
                "id": "24d718d7-0301-4e9d-a333-b87e8694a1d4",
                "name": "job2",
                "status": "stopped",
                "slurm_id": 0,
                "slurm_job": None,
            },
        }

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def _get_job(self, job_id, mock_request):
        mock_request.return_value.status_code = 200
        mock_request.return_value.json.return_value = self.job_infos[job_id]
        return self.app.get_job(job_id)

    def test_str(self):
        job = self._get_job("b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        self.assertEqual(
            str(job),
            "Job job1 (b335e27d-18d9-4612-b1a1-d3a522ef6c16) [App test (10ec28fb-93be-48c2-ac3d-f3bf464eb2cc)]",
        )

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_job_start(self, mock_request):
        mock_request.return_value.status_code = 201
        mock_request.return_value.json.return_value = {
            "id": "b335e27d-18d9-4612-b1a1-d3a522ef6c16",
            "name": "job1",
            "status": "stopped",
            "slurm_id": 0,
            "slurm_job": None,
        }
        job = self.app.create_job("srun", ["-N", "1"], {"time": "1:00:00"})
        self.assertEqual(str(job.id), "b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        self.assertEqual(job.name, "job1")
        self.assertEqual(job.status, "stopped")

        mock_request.return_value.status_code = 200
        mock_request.return_value.json.return_value = {
            "job_id": 1234,
            "name": "job1",
            "job_state": "RUNNING",
            "state_description": "Job is running",
            "state_reason": "",
            "account": "test",
            "user_name": "test",
        }
        jstatus = job.start()
        mock_request.assert_called_with(
            f"jobs/{job.id}/start",
            "PUT",
            data=None,
            file_data=None,
            stream=False,
            verbose=False,
        )
        self.assertEqual(jstatus, job.slurm_job)
        self.assertEqual(jstatus.job_id, 1234)
        self.assertEqual(jstatus.name, "job1")
        self.assertEqual(jstatus.job_state, "RUNNING")

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_job_refresh(self, mock_request):
        job = self._get_job("b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        mock_request.return_value.status_code = 200
        mock_request.return_value.json.return_value = self.job_infos[
            "b335e27d-18d9-4612-b1a1-d3a522ef6c16"
        ]
        jstatus = job.refresh()
        mock_request.assert_called_with(
            f"jobs/{job.id}", "GET", stream=False, verbose=False
        )
        self.assertEqual(jstatus, job.slurm_job)
        self.assertEqual(jstatus.job_id, 1234)
        self.assertEqual(jstatus.name, "job1")
        self.assertEqual(jstatus.job_state, "RUNNING")
        self.assertTrue(jstatus.is_running())
        self.assertFalse(jstatus.is_terminated())
        self.assertFalse(jstatus.is_failed())

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_job_refresh_no_slurm_job(self, mock_request):
        job = self._get_job("24d718d7-0301-4e9d-a333-b87e8694a1d4")
        mock_request.return_value.status_code = 200
        mock_request.return_value.json.return_value = self.job_infos[
            "24d718d7-0301-4e9d-a333-b87e8694a1d4"
        ]
        jstatus = job.refresh()
        mock_request.assert_called_with(
            f"jobs/{job.id}", "GET", stream=False, verbose=False
        )
        self.assertIsNone(jstatus)

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_job_get_stdout(self, mock_request):
        job = self._get_job("b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        mock_request.return_value.status_code = 200
        mock_request.return_value.text = "hello world\n"
        jlog = job.get_stdout()
        mock_request.assert_called_with(
            f"jobs/{job.id}/out", "GET", stream=False, verbose=False
        )
        self.assertEqual(jlog, "hello world\n")

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_job_get_batch(self, mock_request):
        job = self._get_job("b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        mock_request.return_value.status_code = 200
        mock_request.return_value.text = "#!/bin/bash\n\necho hello world\n"
        jlog = job.get_batch()
        mock_request.assert_called_with(
            f"jobs/{job.id}/batch", "GET", stream=False, verbose=False
        )
        self.assertEqual(jlog, "#!/bin/bash\n\necho hello world\n")

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_job_push_files(self, mock_request):
        job = self._get_job("b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        mock_request.return_value.status_code = 200
        with tempfile.NamedTemporaryFile() as f:
            job.push_files(f.name)
        mock_request.assert_called_with(
            f"jobs/{job.id}/files",
            "POST",
            data=None,
            file_data=mock.ANY,
            stream=False,
            verbose=False,
        )

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_job_get_files(self, mock_request):
        job = self._get_job("b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        mock_request.return_value.status_code = 200
        mock_request.return_value.content = "undecipherable binary data"
        jfiles = job.get_files()
        mock_request.assert_called_with(
            f"jobs/{job.id}/files", "GET", stream=True, verbose=False
        )
        self.assertEqual(jfiles.content, "undecipherable binary data")

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_job_stop(self, mock_request):
        job = self._get_job("b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        mock_request.return_value.status_code = 204
        jstatus = job.stop()
        mock_request.assert_called_with(
            f"jobs/{job.id}/stop",
            "PUT",
            data=None,
            file_data=None,
            stream=False,
            verbose=False,
        )
        self.assertTrue(jstatus)

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_job_prune(self, mock_request):
        job = self._get_job("b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        mock_request.return_value.status_code = 204
        jstatus = job.prune()
        mock_request.assert_called_with(
            f"jobs/{job.id}/prune",
            "PUT",
            data=None,
            file_data=None,
            stream=False,
            verbose=False,
        )
        self.assertTrue(jstatus)

    @mock.patch("slurmer.provider.SlurmerProvider.request")
    def test_job_delete(self, mock_request):
        job = self._get_job("b335e27d-18d9-4612-b1a1-d3a522ef6c16")
        mock_request.return_value.status_code = 204
        jstatus = job.delete()
        mock_request.assert_called_with(
            f"jobs/{job.id}", "DELETE", verbose=False
        )
        self.assertTrue(jstatus)
