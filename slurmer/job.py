from dataclasses import dataclass
from datetime import datetime, timedelta
from uuid import UUID
from typing import TYPE_CHECKING
import requests

from .constants import (
    JOBSTATUS_TERMINATED,
    JOBSTATUS_RUNNING,
    JOBSTATUS_ERROR,
    JOBSTATUS_WAITING,
)

from .provider import SlurmerError

if TYPE_CHECKING:
    from .app import SlurmerApp

__all__ = ["SlurmJob", "SlurmerJob", "SlurmerJobNotFound"]

class SlurmerJobNotFound(SlurmerError):
    pass

@dataclass(frozen=True)
class SlurmJob:
    """
    A frozen dataclass that represents the state of a *Slurm* job.
    """

    job_id: int
    job_state: str
    state_description: str
    state_reason: str
    account: str
    name: str
    user_name: str
    submit_time: datetime | None = None
    start_time: datetime | None = None
    end_time: datetime | None = None

    def is_running(self) -> bool:
        return self._is_in(JOBSTATUS_RUNNING)

    def is_terminated(self) -> bool:
        return self._is_in(JOBSTATUS_TERMINATED)

    def is_failed(self) -> bool:
        return self._is_in(JOBSTATUS_ERROR)

    def _is_in(self, states: frozenset[str]) -> bool:
        return self.job_state in states
    
    @property
    def duration(self) -> timedelta | None:
        if self.start_time and self.end_time:
            return self.end_time - self.start_time
        return None

    @classmethod
    def from_dict(cls, data: dict) -> "SlurmJob":
        return cls(
            data["job_id"],
            data["job_state"],
            data["state_description"],
            data["state_reason"],
            data["account"],
            data["name"],
            data["user_name"],
            datetime.fromtimestamp(data["submit_time"]) if data.get("submit_time", None) else None,
            datetime.fromtimestamp(data["start_time"]) if data.get("start_time", None) else None,
            datetime.fromtimestamp(data["end_time"]) if data.get("end_time", None) else None,
        )


@dataclass
class SlurmerJob:
    """
    Main class to represent a job in the Slurmer API.
    """

    app: "SlurmerApp"
    id: UUID
    name: str
    status: str
    slurm_id: int
    slurm_job: SlurmJob | None

    @property
    def endpoint(self) -> str:
        return f"jobs/{self.id}"

    def __str__(self) -> str:
        return f"Job {self.name} ({self.id}) [{self.app}]"

    def refresh(self) -> SlurmJob | None:
        """
        Refresh the job status.

        Updates the object with the latest status.
        Returns the *slurm* job info.
        """
        resp = self.app.provider.get(self.endpoint)
        
        if resp.status_code == 404:
            raise SlurmerJobNotFound(f"Job {self.id} not found.")
        elif resp.status_code != 200:
            raise SlurmerError(f"Unable to refresh job {self.id}. Status code {resp.status_code}")
        
        job_data = resp.json()
        self.status = job_data["status"]
        self.slurm_id = job_data["slurm_id"]
        self.slurm_job = (
            SlurmJob.from_dict(job_data["slurm_job"])
            if job_data["slurm_job"]
            else None
        )

        return self.slurm_job

    def start(self) -> SlurmJob:
        """
        Start the job.

        Updates the job status, and returns the *slurm* job info.
        """
        endpoint = f"{self.endpoint}/start"
        resp = self.app.provider.put(endpoint)

        if resp.status_code != 200:
            raise Exception(f"Failed to start job: {resp.text}")

        self.slurm_job = SlurmJob.from_dict(resp.json())
        self.slurm_id = self.slurm_job.job_id
        return self.slurm_job

    def stop(self) -> bool:
        """
        Stop the job.

        Returns True if the job was stopped successfully.
        Does not update the object.
        """
        endpoint = f"{self.endpoint}/stop"
        resp = self.app.provider.put(endpoint)
        return resp.status_code == 204

    def delete(self) -> bool:
        """
        Delete the job.

        Returns True if the job was deleted successfully.
        Does not update the object.
        """
        resp = self.app.provider.delete(self.endpoint)
        return resp.status_code == 204

    def prune(self) -> bool:
        """
        Prune the job.

        Returns True if the job was pruned successfully.
        Does not update the object.
        """
        resp = self.app.provider.put(f"{self.endpoint}/prune")
        return resp.status_code == 204

    def get_batch(self) -> str:
        """
        Get the job's batch script.
        """
        resp = self.app.provider.get(f"{self.endpoint}/batch")
        return resp.text

    def get_stdout(self) -> str:
        """
        Get the job's standard output (after it was executed).
        """
        resp = self.app.provider.get(f"{self.endpoint}/out")
        return resp.text

    def get_files(self) -> requests.Response:
        """
        Get the job's *output* files, in a zip.
        """
        resp = self.app.provider.get(f"{self.endpoint}/files", stream=True)
        return resp

    def push_files(self, local_zip_path: str) -> None:
        """
        Push a zip file to the job's directory.
        """
        post_files = self.app.provider.post(
            f"{self.endpoint}/files",
            file_data={
                "job_dir": (
                    "job.zip",
                    open(local_zip_path, "rb"),
                    "application/zip",
                )
            },
        )
        if post_files.status_code != 200:
            raise SlurmerError(
                f"Unable to push data to slurmer job {post_files}"
            )

    @classmethod
    def from_dict(cls, app: "SlurmerApp", data: dict) -> "SlurmerJob":
        return cls(
            app=app,
            id=UUID(data["id"]),
            status=data["status"],
            name=data["name"],
            slurm_id=data["slurm_id"],
            slurm_job=SlurmJob.from_dict(data["slurm_job"])
            if data["slurm_job"]
            else None,
        )
