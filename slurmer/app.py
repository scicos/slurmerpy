import logging
from typing import Optional, Any
from dataclasses import dataclass
from uuid import UUID
import requests

from .provider import SlurmerProvider, SlurmerError
from .job import SlurmerJob, SlurmerJobNotFound
from .constants import CONNECTION_ERRORS

__all__ = ["SlurmerApp", "SlurmerAppInfo"]


@dataclass(frozen=True)
class SlurmerAppInfo:
    id: str
    name: str
    options: list[str]
    take_command_args: bool

    @classmethod
    def from_dict(cls, data: dict) -> "SlurmerAppInfo":
        return cls(
            data["id"],
            data["name"],
            data["options"],
            data["take_command_args"],
        )


class SlurmerApp:
    """
    A class that represents a Slurmer app.
    """

    def __init__(self, hostname: str, name: str, access_token: str, id: str):
        self.name = name
        self.access_token = access_token
        self.id = id
        self.provider = SlurmerProvider(
            hostname, base_path=self.endpoint, token=access_token
        )

    @property
    def endpoint(self) -> str:
        return f"apps/{self.id}"

    def __str__(self) -> str:
        return f"App {self.name} ({self.id})"

    def is_up(self) -> bool:
        """
        Check if the app is up and the config is correct.
        """
        try:
            app_info = self.get_app_info()
            logging.debug(app_info)
            if app_info is not None:
                return app_info.id == self.id
            else:
                return False
        except CONNECTION_ERRORS:
            return False

    def get_app_info(self) -> Optional[SlurmerAppInfo]:
        """
        Get the app info.
        """
        app_resp = self.provider.get()
        if app_resp.status_code == 200:
            app_data = app_resp.json()
            return SlurmerAppInfo.from_dict(app_data)
        elif app_resp.status_code != 404:
            app_resp.raise_for_status()
        return None

    def create_job(
        self,
        command: Optional[str] = None,
        args: list[str] = [],
        options: dict[str, str | int | float] = {},
        job_name: str = "",
        comment: str = "",
    ) -> SlurmerJob:
        """
        Create a job in the Slurmer app.

        Args:
            command (str, optional): The command to run. Needed only if take_command_args is True.
            args (list[str], optional): The command arguments. Needed only if take_command_args is True.
            options (dict[str, str | int], optional): The job options. Defaults to {}.
            job_name (str, optional): The job name. Defaults to "".
            comment (str, optional): A comment for the job. Defaults to "".
        """
        job_resp = self.provider.post(
            endpoint="jobs",
            data={
                "command": command,
                "args": args,
                "job_name": job_name,
                "comment": comment,
                "options": options,
            },
        )

        if job_resp.status_code != 201:
            raise Exception(
                f"Unable to create job. Status code {job_resp.status_code}"
            )
        else:
            job_data = job_resp.json()
            return SlurmerJob.from_dict(self, job_data)

    def get_job(self, job_uuid: str | UUID) -> SlurmerJob:
        """
        Get a job from the Slurmer app.
        """
        job_resp = self.provider.get(f"/jobs/{job_uuid}")

        if job_resp.status_code == 200:
            return SlurmerJob.from_dict(self, job_resp.json())
        elif job_resp.status_code == 404:
            raise SlurmerJobNotFound(f"Job {job_uuid} not found.")
        else:
            raise SlurmerError(
                f"Unable to get job {job_uuid}. Status code {job_resp.status_code}"
            )

    def get_all_jobs(self) -> Optional[dict[str, SlurmerJob]]:
        """
        Get all jobs from the Slurmer app.
        """
        jobs_resp = self.provider.get("jobs")
        if jobs_resp.status_code == 200:
            jobs_array = jobs_resp.json()
            return dict(
                (state["id"], SlurmerJob.from_dict(self, state))
                for state in jobs_array
            )
        elif jobs_resp.status_code == 404:
            return None
        else:
            raise SlurmerError(f"Status code {jobs_resp.status_code}")

    # Shortcut
    def start_job(
        self,
        srun_cmd: str,
        cmd_args: list[str] = [],
        options: dict[str, str | int | float] = {},
        job_name: str = "",
        file_path: Optional[str] = None,
    ) -> SlurmerJob:
        """
        Shortcut to create a job and start it.
        """
        job = self.create_job(srun_cmd, cmd_args, options, job_name)
        if file_path is not None:
            job.push_files(file_path)
        job.start()
        return job
