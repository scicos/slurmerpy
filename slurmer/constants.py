import requests

# JOB STATUS TYPOLOGY

JOBSTATUS_RUNNING: frozenset[str] = frozenset(
    ["COMPLETING", "RUNNING", "REVOKED", "STAGE_OUT"]
)

JOBSTATUS_WAITING: frozenset[str] = frozenset(
    [
        "CONFIGURING",
        "PENDING",
        "RESV_DEL_HOLD",
        "REQUEUE_FED",
        "REQUEUE_HOLD",
        "REQUEUED",
        "RESIZING",
        "SIGNALING",
        "SPECIAL_EXIT",
    ]
)

JOBSTATUS_TERMINATED: frozenset[str] = frozenset(
    ["CANCELLED", "COMPLETED", "DEADLINE"]
)

JOBSTATUS_ERROR: frozenset[str] = frozenset(
    [
        "BOOT_FAIL",
        "FAILED",
        "NODE_FAIL",
        "OUT_OF_MEMORY",
        "STOPPED",
        "SUSPENDED",
        "TIMEOUT",
    ]
)

# ERRORS

CONNECTION_ERRORS = (ConnectionError, requests.exceptions.ConnectionError)
