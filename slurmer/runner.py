import os
import json
import logging
from typing import Any, Optional
from dataclasses import dataclass

from .provider import SlurmerProvider
from .app import SlurmerApp

__all__ = ["Slurmer"]


def _check_config_data(data: dict, keys: list[str]) -> None:
    missing_keys = [key for key in keys if key not in data]
    if len(missing_keys) > 0:
        raise KeyError(f"Missing keys in config: {missing_keys}")


@dataclass
class Slurmer:
    """
    Main wrapper class for the Slurmer API.
    """

    hostname: str
    apps: dict[str, SlurmerApp]

    @classmethod
    def from_dict(cls, dict_data: dict) -> "Slurmer":
        """
        Create a Slurmer object from a dictionary.

        The dictionary should have the following structure:

        ```json
        {
            "hostname": "https://slurmerurl:port/",
            "apps": [
                {
                    "name": "app_name",
                    "access_token": "token",
                    "id": "id"
                }
            ]
        }
        ```
        """
        _check_config_data(dict_data, ["hostname", "apps"])

        hostname = dict_data["hostname"]
        apps = {}
        for app_data in dict_data["apps"]:
            _check_config_data(app_data, ["access_token", "id", "name"])

            apps[app_data["name"]] = SlurmerApp(
                hostname=hostname,
                name=app_data["name"],
                access_token=app_data["access_token"],
                id=app_data["id"],
            )

        return cls(hostname, apps)

    @classmethod
    def from_config(cls, file_path: str) -> "Slurmer":
        """
        Create a Slurmer object from a JSON file.

        See `from_dict` for the expected structure of the JSON file.
        """
        if os.path.isfile(file_path):
            logging.info(f"Reading Slurmer config from {file_path}")

            with open(file_path, "r") as f:
                json_data = json.load(f)

            return cls.from_dict(json_data)
        else:
            raise IOError(f"Config file not found: {file_path}")
