import logging
from typing import Optional, Any
import requests
from dataclasses import dataclass

__all__ = ["SlurmerProvider", "SlurmerError"]

# === START DEBUGGING REQUEST ===
# import logging
# logging.basicConfig()
# logging.getLogger().setLevel(logging.DEBUG)
# requests_log = logging.getLogger("requests.packages.urllib3")
# requests_log.setLevel(logging.DEBUG)
# requests_log.propagate = True
# === END DEBUGGING REQUEST ===


class SlurmerError(Exception):
    pass


@dataclass(frozen=True)
class SlurmerProvider:
    """
    SlurmerProvider is a class that provides a way to interact with the Slurmer API.
    """

    hostname: str
    base_path: str = ""
    token: Optional[str] = None

    def __str__(self) -> str:
        return f"Provider {self.hostname}"

    def request(
        self,
        endpoint: str,
        method: str,
        data: Optional[dict] = None,
        file_data: Any = None,
        stream: bool = False,
        verbose=False,
    ) -> requests.Response:
        prefix = f"{self.hostname}/{self.base_path}/{endpoint}"
        url = prefix.replace("//", "/").replace(":/", "://")
        resp = requests.request(
            method,
            url,
            headers={
                "X-Auth-Token": self.token,
            },
            json=data,
            files=file_data,
            stream=stream,
        )

        if verbose:
            logging.info(
                f"Request {method} to {url} returned {resp.status_code}"
            )
            if resp.request.body:
                if isinstance(resp.request.body, bytes):
                    str_body = resp.request.body.decode("utf-8")
                else:
                    str_body = resp.request.body
                logging.info(f"Request body: {str_body[:1000]}")
            if resp.text:
                logging.info(f"Request response: {resp.text[:1000]}")
        return resp

    def get(
        self,
        endpoint: str = "",
        stream: bool = False,
        verbose: bool = False,
    ) -> requests.Response:
        return self.request(endpoint, "GET", stream=stream, verbose=verbose)

    def post(
        self,
        endpoint: str,
        data: Optional[dict] = None,
        file_data: Any = None,
        stream: bool = False,
        verbose: bool = False,
    ) -> requests.Response:
        return self.request(
            endpoint,
            "POST",
            data=data,
            file_data=file_data,
            stream=stream,
            verbose=verbose,
        )

    def put(
        self,
        endpoint: str,
        data: Optional[dict] = None,
        file_data: Any = None,
        stream: bool = False,
        verbose=False,
    ) -> requests.Response:
        return self.request(
            endpoint,
            "PUT",
            data=data,
            file_data=file_data,
            stream=stream,
            verbose=verbose,
        )

    def delete(
        self,
        endpoint: str,
        verbose=False,
    ) -> requests.Response:
        return self.request(endpoint, "DELETE", verbose=verbose)
