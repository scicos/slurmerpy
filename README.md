# Slurmerpy

Slurmerpy is a python package meant to provide a easy integration with Slurmer.

## Usage

```python
# Create the main runner (must match the slurmer config file)
runner = Slurmer.from_dict({
    "hostname": "http://localhost:8080",
    "apps": [
        {
            "name": "test",
            'access_token': 'PssEpTGgFcfAspkqudabvi3UjCSRRbfLBGBhGa2f428=',
            "id": "10ec28fb-93be-48c2-ac3d-f3bf464eb2cc"
        }
    ]
})
test_app = runner.apps["test"]

# Check that it works
assert test_app.is_up()

print(test_app.get_app_info())

# Create a job
new_job = test_app.create_job("echo", ["hello world"], options={"time": 10})

# Push some input files
new_job.push_files("a-zip-file.zip")

# Start the job
new_job.start()

# Refresh status
slurmjobstatus = new_job.refresh()

# Check status
print(slurmjobstatus.is_running())

# Shortcut to create a job
new_job2 = test_app.start_job("echo", ["hello world"], )

# Or get a previous job
job = test_app.get_job("6276d2a4-f791-4a42-abc5-1ae49effa9bd")

# Download output files for the job's outputs/ dir
with open("outputs.zip" , "wb") as f:
    f.write(new_job.get_files().content)

# Prune results
new_job.prune()

# Totally delete + prune job
new_job.delete()
```