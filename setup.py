from setuptools import setup

setup(
    name="slurmerpy",
    py_modules=["slurmer"],
    version="0.1",
    description="Wrapper for the Slurmer API.",
    author="Christophe Charpilloz, Robin Champenois",
    packages=["slurmer"],
    install_requires=[
        "requests",
    ],
    include_package_data=True,
    extras_require={"dev": ["pytest", "mypy", "ruff"]},
)
